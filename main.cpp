#include <iostream>
#include <memory>
#include <vector>
#include "NN.h"

int main(int argc, char** argv) {

	std::vector<int> vec;
	vec.push_back(2);
	vec.push_back(4);
	vec.push_back(1);
	NN nn(vec);

	std::vector<double> xor_11;
	std::vector<double> xor_10;
	std::vector<double> xor_01;
	std::vector<double> xor_00;

	xor_11.push_back(1);
	xor_11.push_back(1);
	xor_10.push_back(1);
	xor_10.push_back(0);
	xor_01.push_back(0);
	xor_01.push_back(1);
	xor_00.push_back(0);
	xor_00.push_back(0);

	std::vector<double> wished_11;
	std::vector<double> wished_10;
	std::vector<double> wished_01;
	std::vector<double> wished_00;

	wished_11.push_back(0);
	wished_10.push_back(1);
	wished_01.push_back(1);
	wished_00.push_back(0);
	for (uint32_t i = 0; i < 5000; i++) {
		std::cout << "error rate: " << nn.train(xor_11, wished_11) << std::endl;
		std::cout << "error rate: " << nn.train(xor_10, wished_10) << std::endl;
		std::cout << "error rate: " << nn.train(xor_01, wished_01) << std::endl;
		std::cout << "error rate: " << nn.train(xor_00, wished_00) << std::endl;
	}

	const std::vector<double>& result_11 = nn.get_output(xor_11);
	std::cout << "result_11: " << result_11[ 0 ] << std::endl;
	const std::vector<double>& result_10 = nn.get_output(xor_10);
	std::cout << "result_10: " << result_10[ 0 ] << std::endl;
	const std::vector<double>& result_01 = nn.get_output(xor_01);
	std::cout << "result_01: " << result_01[ 0 ] << std::endl;
	const std::vector<double>& result_00 = nn.get_output(xor_00);
	std::cout << "result_00: " << result_00[ 0 ] << std::endl;
	
  // Get current weight and biases to initialize a new Neural Network
  // This is important because we can save those values and then not lose our improvement 
	auto w = nn.get_weight();
	auto b = nn.get_biases(); 

	NN nn2(vec, w, b);

	const std::vector<double>& result_x_11 = nn2.get_output(xor_11);
	std::cout << "result_x_11: " << result_x_11[ 0 ] << std::endl;

	return 0;
}














