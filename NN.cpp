#include "NN.h"

NN::NN(const std::vector<int>& layer_size) {
	learning_ratio = 0.37;

	this->num_layers = layer_size.size();
	this->layer_size = layer_size;

	srand(time(NULL));

	for (uint16_t i = 0; i < this->num_layers; i++) {

		uint32_t current_layer_size = layer_size[ i ];
		std::vector<double> current_layer;
		std::vector<double> current_delta;

		for (uint32_t j = 0; j < current_layer_size; j++) {
			current_layer.push_back( 0 );
			current_delta.push_back( 0 );
		}

		layer.push_back(current_layer);
		delta.push_back(current_delta);
	}

	for (uint16_t i = 0; i < this->num_layers - 1; i++) {

		uint32_t current_layer_size = layer_size[ i ];
		uint32_t next_layer_size = layer_size[ i + 1 ];
		
		std::vector<std::vector<double> > current_layer;
		for (uint32_t j = 0; j < current_layer_size; j++) {

			std::vector<double> current_neuron;
			for (uint32_t k = 0; k < next_layer_size; k++)
				current_neuron.push_back( get_random() );

			current_layer.push_back(current_neuron);
		}
		
		weight.push_back(current_layer);
	}

	for (uint16_t i = 1; i < this->num_layers; i++) {
		
		uint32_t current_layer_size = layer_size[ i ];
		std::vector<double> current_layer;
		for (uint16_t i = 0; i < current_layer_size; i++)
			current_layer.push_back( get_random() );

		biases.push_back(current_layer);
	}
}

NN::NN(const std::vector<int>& layer_size,
	const std::vector<std::vector<std::vector<double> > >& weight,
	const std::vector<std::vector<double> >& biases) {

	learning_ratio = 0.37;

	this->num_layers = layer_size.size();
	this->layer_size = layer_size;

	this->weight = weight;
	this->biases = biases;

	for (const int& v : layer_size) {
		std::vector<double> current_layer;
		for (uint32_t i = 0; i < v; i++) {
			current_layer.push_back(0);
		}
		layer.push_back(current_layer);
		delta.push_back(current_layer);
	}

	srand(time(NULL));
}

const std::vector<double>& NN::get_output(const std::vector<double>& input) {
	
	if (layer_size[ 0 ] != input.size())
		throw std::runtime_error("input data doesn't match with size of input neurons");

	layer[ 0 ] = input;

	calculate();

	return layer[ this->num_layers - 1 ];
}

double NN::train(const std::vector<double>& input, const std::vector<double>& wished_output) {
	if (layer_size[ 0 ] != input.size())
		throw std::runtime_error("input data training doesn't match with size of input neurons");

	if (layer_size[ num_layers - 1 ] != wished_output.size())
		throw std::runtime_error("wished output data training doesn't match with size of output neurons");

	const std::vector<double>& output = get_output(input);

	uint16_t last_layer = this->num_layers - 1;
	for (uint16_t i = 0; i < this->layer_size[ last_layer ]; i++) {
		delta[ last_layer ][ i ] = layer[ last_layer ][ i ] * (1 - layer[ last_layer ][ i ]) * (layer[ last_layer ][ i ] - wished_output[ i ]);
	}

	for (uint16_t i = this->num_layers - 1; i > 0; i--) {
		for (uint16_t j = 0; j < this->layer_size[ i - 1 ]; j++) {
			delta[ i - 1 ][ j ] = 0;
			for (uint16_t k = 0; k < this->layer_size[ i ]; k++) {
				weight[ i - 1 ][ j ][ k ] -= layer[ i - 1 ][ j ] * delta[ i ][ k ] * learning_ratio;
				delta[ i - 1 ][ j ] += weight[ i - 1 ][ j ][ k ] * delta[ i ][ k ];
			}
		}

		for (uint16_t j = 0; j < this->layer_size[ i - 1 ]; j++) {
			delta[ i - 1 ][ j ] *= layer[ i - 1 ][ j ] * (1 - layer[ i - 1 ][ j ]);
		}

		for (uint16_t j = 0; j < this->layer_size[ i ]; j++) {
			biases[ i - 1 ][ j ] -= delta[ i ][ j ] * learning_ratio;
		}
	}

	return get_error_rate(output, wished_output);
}

const std::vector<std::vector<std::vector<double> > >& NN::get_weight() {
	return weight;
}

const std::vector<std::vector<double> >& NN::get_biases() {
	return biases;
}

// private
double NN::get_random() {
	return ((double)rand() / (double)RAND_MAX);
}

double NN::sigmoid_prime(double x) {
	double sig = sigmoid(x);
	return sig * (1.0 - sig);
}

double NN::sigmoid(double x) {
	return 1.0 / (1.0 + pow(E_CONSTANT, -x));
}

void NN::calculate() {
	for (uint16_t i = 1; i < this->num_layers; i++) {

		uint32_t last_layer_size = layer_size[ i - 1 ];
		uint32_t current_layer_size = layer_size[ i ];

		for (uint32_t j = 0; j < current_layer_size; j++) {
			layer[ i ][ j ] = 0;
			for (uint32_t k = 0; k < last_layer_size; k++)
				layer[ i ][ j ] += layer[ i - 1 ][ k ] * weight[ i - 1 ][ k ][ j ];
			
			layer[ i ][ j ] = sigmoid(layer[ i ][ j ] + biases[ i - 1 ][ j ]);
		}

	}
}

double NN::get_error_rate(const std::vector<double>& output, const std::vector<double>& wished_output) {
	if (output.size() != wished_output.size())
		throw std::runtime_error("can't calculate error ratio with diferente sizes values");

	double error_ratio = 0;
	for (uint16_t i = 0; i < output.size(); i++)
		error_ratio += pow(wished_output[ i ] - output[ i ], 2);

	return sqrt(error_ratio);

}





