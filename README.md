# Neural Network test

This is a convolutional neural network that learns with backpropagation algorithm

it was developed for learning purposes

it runs a classical challenge to learn to use an XOR logic gate

```
1 1 = 0
1 0 = 1
0 1 = 1
0 0 = 1
```
