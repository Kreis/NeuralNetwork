#include <iostream>
#include <memory>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <exception>
#define E_CONSTANT 2.7182818284590452353602874713527

class NN {
public:
	NN(const std::vector<int>& layer_size);
	NN(const std::vector<int>& layer_size,
		const std::vector<std::vector<std::vector<double> > >& weight,
		const std::vector<std::vector<double> >& biases);

	const std::vector<double>& get_output(const std::vector<double>& input);
	double train(const std::vector<double>& input, const std::vector<double>& wished_output);
	const std::vector<std::vector<std::vector<double> > >& get_weight();
	const std::vector<std::vector<double> >& get_biases();

private:
	double learning_ratio;
	uint16_t num_layers;
	std::vector<int> layer_size;

	std::vector<std::vector<double> > layer;
	std::vector<std::vector<std::vector<double> > > weight;
	std::vector<std::vector<double> > biases;
	std::vector<std::vector<double> > delta;

	inline double get_random();
	inline double sigmoid(double x);
	inline double sigmoid_prime(double x);

	void calculate();

	double get_error_rate(const std::vector<double>& output, const std::vector<double>& wished_output);
};

